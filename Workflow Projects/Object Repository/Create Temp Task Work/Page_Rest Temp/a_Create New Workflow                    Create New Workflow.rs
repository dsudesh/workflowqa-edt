<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Create New Workflow                    Create New Workflow</name>
   <tag></tag>
   <elementGuidId>c7ef05a0-707f-4847-b1df-68f6a225aab8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//nav[@id='workflowTemplateActions']/ul/li[2]/a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-primary</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/wkf/workflows/add?templateWorkflowID=386&amp;returnUrl=CfDJ8Bs-hiSgmi9EoANiHkufOHlUTp3eZs0bxfh-zCiZ0t_m34Twe8FEQTbLK0URrIhUjCVKKkVpBTIgzhPufpMaoPvZ5MGwIVRf0zCSIdBcP76BUFloPFuOL7QF4ZWwwKMl3538vBcXQCuwlCAWMsTnlTg</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-labelledby</name>
      <type>Main</type>
      <value>e8a69799-tooltip</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        Create New Workflow
                    Create New Workflow</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;workflowTemplateActions&quot;)/ul[1]/li[2]/a[@class=&quot;btn btn-primary&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//nav[@id='workflowTemplateActions']/ul/li[2]/a</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Unpublish'])[2]/following::a[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Unpublish'])[1]/following::a[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '/wkf/workflows/add?templateWorkflowID=386&amp;returnUrl=CfDJ8Bs-hiSgmi9EoANiHkufOHlUTp3eZs0bxfh-zCiZ0t_m34Twe8FEQTbLK0URrIhUjCVKKkVpBTIgzhPufpMaoPvZ5MGwIVRf0zCSIdBcP76BUFloPFuOL7QF4ZWwwKMl3538vBcXQCuwlCAWMsTnlTg')]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//nav[4]/ul/li[2]/a</value>
   </webElementXpaths>
</WebElementEntity>
