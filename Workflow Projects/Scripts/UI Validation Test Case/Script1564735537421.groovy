import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('https://triton.debtx.com/wkf/')

WebUI.click(findTestObject('Object Repository/UI/Page_Home/a_Workflows                Workflows'))

WebUI.setText(findTestObject('Object Repository/UI/Page_Workflows/input_Workflows_searchText'), 'car')

WebUI.click(findTestObject('Object Repository/UI/Page_Workflows/span_Filter'))

WebUI.click(findTestObject('Object Repository/UI/Page_Workflows/span_Add New'))

WebUI.click(findTestObject('Object Repository/UI/Page_Workflows/a_Cancel'))

WebUI.closeBrowser()

WebUI.openBrowser('https://triton.debtx.com/wkf/')

WebUI.click(findTestObject('Object Repository/UI/Page_Home/span_Templates'))

WebUI.setText(findTestObject('Object Repository/UI/Page_Templates/input_Templates_searchText'), 'temp')

WebUI.click(findTestObject('Object Repository/UI/Page_Templates/span_Filter'))

WebUI.click(findTestObject('Object Repository/UI/Page_Templates/span_Add New'))

WebUI.click(findTestObject('Object Repository/UI/Page_Templates/a_Cancel'))

WebUI.closeBrowser()

